# Web Scraper using Playwright

This is a simple Python script that uses the Playwright library to scrape data from a website. The script launches a Playwright instance, navigates to a specific webpage, captures JSON responses, and saves the results to a JSON file.

## Prerequisites

- Python 3.x
- Playwright library

## Installation

1. Clone the repository:

   ```shell
   git clone https://gitlab.com/iwannabe.dev/web-scraping-json.git
   ```

2. Install the required dependencies:

   ```shell
   pip install playwright
   ```

3. Install the Playwright browser binaries:

    ```shell
    playwright install
    ```

## Usage

1. Open the script file (`main.py`) in a text editor of your choice.

2. Modify the following lines in `main.py` to suit your scraping needs:

   - URL: Update the `page.goto()` line with the desired website URL.
   - JSON response handling: Customize the `test_json()` function as per your requirements.

3. Run the script:

   ```shell
   python main.py
   ```

4. The script will open a browser window (in non-headless mode) and navigate to the specified URL. It will capture JSON responses from the webpage and save the results to a `results.json` file.

## Notes

- Adjust the script as needed to match the structure and behavior of the website you are scraping.
- Be aware of the website's terms of service and avoid sending excessive requests to prevent any potential issues.