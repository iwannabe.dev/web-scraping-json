#################################################################################
##  This is a simple Python script that uses the Playwright library to scrape  ##
##  data from a website. The script launches a Playwright instance, navigates  ##
##  to a specific webpage, captures JSON responses, and saves the results to   ##
##  a JSON file.                                                               ##
#################################################################################


from playwright.sync_api import sync_playwright
import json


def test_json(response, results):
    try:
        results.append(
            {
                'url': response.url,
                'data': response.json(),
            }
        )
    except:
        # bad
        pass

def run(playwright):
    results = []
    chromium = playwright.chromium
    browser = chromium.launch(headless=False)
    page = browser.new_page()
    page.on("response", lambda response: test_json(response, results))
    page.goto("http://nba.com/stats/players")
    browser.close()

    return results


with sync_playwright() as playwright:
    data = run(playwright)
    with open("results.json", "w") as f:
        json.dump(data, f)

